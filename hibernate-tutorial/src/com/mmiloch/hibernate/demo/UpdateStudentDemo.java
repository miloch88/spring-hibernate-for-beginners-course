package com.mmiloch.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Student;

public class UpdateStudentDemo {

	public static void main(String[] args) {
		
		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {
			
			int studentId =1;
			
			session = factory.getCurrentSession();
			session.beginTransaction();

			System.out.println("Getting student with id: " + studentId);
			Student myStudent = session.get(Student.class, studentId);
			System.out.println("Updating student...");
			
			myStudent.setFirstName("Plaqurebearer");
			
			session.getTransaction().commit();
			

			session = factory.getCurrentSession();
			session.beginTransaction();
			
			System.out.println("Update email for all students...");
			session.createQuery("update Student set email='foo@gmail.com'").executeUpdate();
			
			session.getTransaction().commit();
			
		}finally{
			factory.close();
		}
	}

}
