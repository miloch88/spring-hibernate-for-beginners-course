package com.mmiloch.hibernate.example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Employee;

public class CreateEmployee {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure("hibernate_example.cfg.xml")
				.addAnnotatedClass(Employee.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			Employee myEmployee = new Employee("Jace", "Beleren", "Vryn");
		
			session.beginTransaction();
			
			session.save(myEmployee);
			
			session.getTransaction().commit();
			
		}finally {
			factory.close();
		}
	}

}
