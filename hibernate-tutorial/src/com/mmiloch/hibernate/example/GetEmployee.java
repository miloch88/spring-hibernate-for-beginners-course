package com.mmiloch.hibernate.example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Employee;

public class GetEmployee {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure("hibernate_example.cfg.xml")
				.addAnnotatedClass(Employee.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			int getId = 1;
					
			session.beginTransaction();
			
			Employee myEmployee = session.get(Employee.class, getId);
			System.out.println("GET: " + myEmployee);
			
			session.getTransaction().commit();
			
		}finally {
			factory.close();
		}
	}

}
