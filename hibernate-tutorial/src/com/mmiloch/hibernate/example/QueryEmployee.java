package com.mmiloch.hibernate.example;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Employee;

public class QueryEmployee {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure("hibernate_example.cfg.xml")
				.addAnnotatedClass(Employee.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
							
			session.beginTransaction();
			
			List<Employee> myEmployees = session.createQuery("from Employee e where e.company= 'Vryn'").list();
			
			System.out.println("LIST: " + myEmployees);
			
			session.getTransaction().commit();
			
		}finally {
			factory.close();
		}
	}

}
