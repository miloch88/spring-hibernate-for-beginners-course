package com.mmiloch.aopdemo.dao;

import org.springframework.stereotype.Component;

import com.mmiloch.aopdemo.Account;

@Component
public class AccountDAO {

	public void addAccount(Account theAccount, boolean vipFlag ) {
		
		System.out.println(getClass() + ": Doing my DB work: adding an account");
	}
	
	public boolean doWork() {
		
		System.out.println(getClass() + ": doWork()");
		return false;
	}
}
