package com.mmiloch.springboot.demo.mmapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmappApplication.class, args);
	}

}
