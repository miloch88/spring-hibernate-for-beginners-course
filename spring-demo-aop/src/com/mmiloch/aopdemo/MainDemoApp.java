package com.mmiloch.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mmiloch.aopdemo.dao.AccountDAO;
import com.mmiloch.aopdemo.dao.MembershipDAO;

public class MainDemoApp {

	public static void main(String[] args) {
	
		// read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get the bean from spring container
		AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);
		MembershipDAO membershipDAO = context.getBean("membershipDAO", MembershipDAO.class);
		
		// call the business method
		accountDAO.addAccount();
		
		membershipDAO.addMethod();
		
//		accountDAO.addAccount();
		
		// close the context
		context.close();

	}

}
