package com.mmiloch.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {

	public boolean addMethod() {
		
		System.out.println(getClass() + ": Doing stuff: adding a membership account");
	
		return true;
	}
}
