<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
</head>

<body>

	<form:form action="processForm" modelAttribute="student">
	
		First name: <form:input path="firstName"/>
		
		<br><br>
		
		Last name: <form:input path="lastName"/>
		
		<br><br>
		
		Country: 
	
			<form:select path="country">
				
				<form:options items="${theCountryOptions}"/>
				<!-- <form:options items="${student.countryOptions}"/> -->
				<!-- <form:option value="France" label="France"/> -->
				
			</form:select>	
			
		<br><br>
		
		Favorite Language:

			<!-- Ruby <form:radiobutton path="favouriteLanguage" value="Ruby"/>  -->
			<form:radiobuttons path="favouriteLanguage" items="${student.favoriteLanguageOptions}"  />
		
		<br><br>
		
		Operating Systems:
		
			Linux <form:checkbox path="operatingSystem" value="Lnux"/>
			Mac OS <form:checkbox path="operatingSystem" value="Mac OS"/>
			MS Windows <form:checkbox path="operatingSystem" value="MS Windows"/>
		
		<br><br>
						
		<input type="submit" value="Submit" />
	
	</form:form>

</body>
</html>