package com.mmiloch.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;

public class GetInstrucotrDetailDemo {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
						
			session.beginTransaction();
			
			int theId = 3;
			
			InstructorDetail myInstructorDetail = session.get(InstructorDetail.class, theId);
			
			System.out.println("myInstructorDetail: " + myInstructorDetail);
			
			System.out.println("The associated instructor: " +myInstructorDetail.getInstructor());
			

			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}catch(Exception exc) {
			exc.printStackTrace();
		}finally{
			session.close();
			factory.close();
		}
	}

}
