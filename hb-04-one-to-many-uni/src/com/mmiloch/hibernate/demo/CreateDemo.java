package com.mmiloch.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;

public class CreateDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			Instructor myInstructor = new Instructor("Jace", "Beleren", "jbeleren@mmiloch.com");
			InstructorDetail myInstructorDetail = new InstructorDetail("www.youtube.com/jbeleren", "Blue Mana");
			
			Instructor myInstructor1 = new Instructor("Lilana", "Vess", "lvess@mmiloch.com");
			InstructorDetail myInstructorDetail1 = new InstructorDetail("www.youtube.com/lvess", "Black Mana");
			
			myInstructor.setInstructorDetail(myInstructorDetail);
			myInstructor1.setInstructorDetail(myInstructorDetail1);
			
			session.beginTransaction();
			
			// this will also save the details object because of CascadeType.All
			session.save(myInstructor);
			session.save(myInstructor1);
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}finally{
			factory.close();
		}
	}

}
