package com.mmiloch.springdemo;

public interface Coach {
	
	public String getDailyWorkout();
	
	public String getDailyFortune();
	
}
