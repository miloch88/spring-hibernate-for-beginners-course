package com.mmiloch.springdemo;

public interface FortuneService {
	
	public String getFortune();
	
}
