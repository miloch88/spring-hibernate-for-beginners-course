package com.mmiloch.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Course;
import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;
import com.mmiloch.hibernate.demo.entity.Review;
import com.mmiloch.hibernate.demo.entity.Student;

public class CreateCourseAndReviewsDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();

			Course myCourse = new Course("Casting black mana magic!");
			
			System.out.println("\nSaving the Course ... ");
			session.save(myCourse);
			System.out.println("Saved the course: " + myCourse);
			
			Student myStudent1 = new Student("Kaya", "Ghost-Assassin", "kghost-assassin@mmiloch.com");
			Student myStudent2 = new Student("Saheeli ", "Rai", "srai@mmiloch.com");
			
			myCourse.addStudent(myStudent1);
			myCourse.addStudent(myStudent2);
			
			System.out.println("\nSaving studetns...");
			
			session.save(myStudent1);
			session.save(myStudent2);
			
			System.out.println("Saved students: " + myCourse.getStudents());
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}catch(Exception exc) {
			exc.printStackTrace();
		}
		finally{
			
			session.close();
			factory.close();
		}
	}

}
