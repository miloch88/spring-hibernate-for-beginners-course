package com.mmiloch.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Course;
import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;
import com.mmiloch.hibernate.demo.entity.Review;
import com.mmiloch.hibernate.demo.entity.Student;

public class DeleteBlackCourseDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();

			int courseId = 12;
			Course myCourse = session.get(Course.class, courseId);
			
			System.out.println("Deleting course: " + myCourse);

			session.delete(myCourse);
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
			
		}catch(Exception exc) {
			exc.printStackTrace();
		}
		finally{
			
			session.close();
			factory.close();
		}
	}

}
