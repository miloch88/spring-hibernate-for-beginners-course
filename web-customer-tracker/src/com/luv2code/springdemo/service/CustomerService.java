package com.luv2code.springdemo.service;

import java.util.List;

import com.luv2code.springdemo.entity.Customer;

public interface CustomerService {

	public List<Customer> getCustomers();

	public void saveCustomer(Customer myCustomer);

	public Customer getCustomer(int myId);

	public void deleteCustomer(int myId);

	public List<Customer> searchCustomers(String theSearchName);
}
