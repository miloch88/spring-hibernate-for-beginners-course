package com.mmiloch.aopdemo.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.mmiloch.aopdemo.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {
	
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	@Around("execution(* com.mmiloch.aopdemo.service.*.getFortune(..))") 
	public Object aroundGetFortune(ProceedingJoinPoint theProceedingJoinPoint) throws Throwable{
		
		String method = theProceedingJoinPoint.getSignature().toLongString();
		myLogger.info("\n====>>> Executing @Around (finally) on method" + method);
		
		long begin = System.currentTimeMillis();
		
		Object result = theProceedingJoinPoint.proceed();
		
		long end = System.currentTimeMillis();
		
		long duration = end - begin;
		myLogger.info("\n====> Duration: " + duration/ 1000.0 + " seconds");
		
		return result;
	}
	
	@After("execution(* com.mmiloch.aopdemo.dao.AccountDAO.findAccounts(..))") 
	public void afterFinallyFindAccountsAdvice(JoinPoint theJoinPoint) {
		
		String method = theJoinPoint.getSignature().toLongString();
		myLogger.info("\n====>>> Executing @After (finally) on method" + method);
		
	}
	
	@AfterThrowing(pointcut="execution(* com.mmiloch.aopdemo.dao.AccountDAO.findAccounts(..))", throwing="theExc") 
	public void afterThrowingFindAccountAdvice(JoinPoint myJoinPoint, Throwable theExc ) {
		
		String method = myJoinPoint.getSignature().toLongString();
		myLogger.info("\n====>>> Executing @AfterThrowing on method" + method);
		
		myLogger.info("\n====>>> The ecveption is: " + theExc);
		
	}

	
	@AfterReturning(pointcut="execution(* com.mmiloch.aopdemo.dao.AccountDAO.findAccounts(..))", returning = "result")
	public void afterReturningFindAccountAdvice(JoinPoint myjJoinPoint, List<Account> result) { 
		
		String method = myjJoinPoint.getSignature().toLongString();
		myLogger.info("\n====>>> Executing @AfterReturning on method" + method);
		
		myLogger.info("\n====>>> result is: " + result);
		
		convertAccountNamesToUpperCase(result);
		
	}
	
	private void convertAccountNamesToUpperCase(List<Account> result) {
		
		for (Account tempAccount: result) {
			String theUpperName = tempAccount.getName().toUpperCase();
			
			tempAccount.setName(theUpperName);
		}
		
	}

	@Before("com.mmiloch.aopdemo.aspect.MmilochAopExpressions.forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint theJoinPoint) {		
		myLogger.info("\n=====>>> Executing @Before advice on method");	
		
		// display the method signature
		MethodSignature methodSignature = (MethodSignature) theJoinPoint.getSignature();
		
		myLogger.info("Method: " + methodSignature);
		
		// display method arguments
		
		
		Object[] args = theJoinPoint.getArgs();
		
		for(Object tempArg: args) {
			myLogger.info(tempArg.toString());
			
			if( tempArg instanceof Account) {
				// downcast and print Account stuff
				Account theAccount = (Account) tempArg;
				
				myLogger.info("accoutn name: " + theAccount.getName());
				myLogger.info("accoutn name: " + theAccount.getLevel());
			}
		}
		
	}
	
}
