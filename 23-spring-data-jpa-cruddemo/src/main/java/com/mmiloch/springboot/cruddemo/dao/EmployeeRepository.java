package com.mmiloch.springboot.cruddemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mmiloch.springboot.cruddemo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
