package com.mmiloch.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mmiloch.aopdemo.dao.AccountDAO;
import com.mmiloch.aopdemo.dao.MembershipDAO;

public class MainDemoApp {

	public static void main(String[] args) {
	
		// read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get the bean from spring container
		AccountDAO myAccountDAO = context.getBean("accountDAO", AccountDAO.class);
		MembershipDAO membershipDAO = context.getBean("membershipDAO", MembershipDAO.class);
		
		// call the business method
		Account myAccount = new Account();
		myAccountDAO.addAccount(myAccount, true);
		myAccountDAO.doWork();
		
		myAccountDAO.setName("foobar");
		myAccountDAO.setServiceCode("silver");
		
		String name = myAccountDAO.getName();
		String code = myAccountDAO.getServiceCode();
		
		membershipDAO.addMethod();
		membershipDAO.goToSleep();
		
//		accountDAO.addAccount();
		
		// close the context
		context.close();

	}

}
