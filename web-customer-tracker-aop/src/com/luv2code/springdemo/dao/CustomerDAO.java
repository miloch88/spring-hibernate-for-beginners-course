package com.luv2code.springdemo.dao;

import java.util.List;

import com.luv2code.springdemo.entity.Customer;

public interface CustomerDAO {

	public List<Customer> getCustomers();

	public void saveCustomer(Customer myCustomer);

	public Customer getCustomer(int myId);

	public void deleteCustomer(int myId);

	public List<Customer> searchCustomer(String theSearchName);
	
}
