package com.mmiloch.hibernate.demo;

import java.text.ParseException;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;
import com.mmiloch.hibernate.demo.entity.Student;

public class DeleteDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
						
			session.beginTransaction();
			
			int theId = 1;
			
			Instructor myInstructor = session.get(Instructor.class, theId);
			
			if(myInstructor != null) {
				
				session.delete(myInstructor);
			}
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}finally{
			factory.close();
		}
	}

}
