package com.mmiloch.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Course;
import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;

public class DeleteCourseDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();

			int theId = 10;
			Course myCourse = session.get(Course.class, theId);
			
			System.out.println("Deleting coure: " + myCourse);
			
			session.delete(myCourse);
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}catch(Exception exc) {
			exc.printStackTrace();
		}
		finally{
			
			session.close();
			factory.close();
		}
	}

}
