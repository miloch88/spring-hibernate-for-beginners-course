package com.mmiloch.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mmiloch.hibernate.demo.entity.Course;
import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;

public class CreateCoursesDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();

			int theId = 1;
			Instructor myInstructor = session.get(Instructor.class, theId);
			
			Course myCourse1 = new Course("Cast creatures");
			Course myCourse2 = new Course("Cast sorceryies");
			Course myCourse3 = new Course("Cast instatns");
			
			myInstructor.add(myCourse1);
			myInstructor.add(myCourse2);
			myInstructor.add(myCourse3);
			
			session.save(myCourse1);
			session.save(myCourse2);
			session.save(myCourse3);
			
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}catch(Exception exc) {
			exc.printStackTrace();
		}
		finally{
			
			session.close();
			factory.close();
		}
	}

}
