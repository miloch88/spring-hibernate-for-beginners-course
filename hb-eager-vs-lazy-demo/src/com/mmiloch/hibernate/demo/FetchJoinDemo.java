package com.mmiloch.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.mmiloch.hibernate.demo.entity.Course;
import com.mmiloch.hibernate.demo.entity.Instructor;
import com.mmiloch.hibernate.demo.entity.InstructorDetail;

public class FetchJoinDemo {

	public static void main(String[] args) throws ParseException {
		
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();

			int theId = 1;
			Query<Instructor> query = session.createQuery("select i from Instructor i join fetch i.courses where i.id=:theInstrucotrId", Instructor.class);
			
			query.setParameter("theInstrucotrId", theId);
			
			Instructor myInstructor = query.getSingleResult();
			
			System.out.println("mmiloch: Instructor: " + myInstructor);

			session.getTransaction().commit();
			session.close();
			
			System.out.println("mmiloch: Courses: " + myInstructor.getCourses());
			
			System.out.println("mmiloch: Done!");
		}
//		catch(Exception exc) {
//			exc.printStackTrace();
//		}
		finally{
			
			session.close();
			factory.close();
		}
	}

}
