package com.mmiloch.aopdemo;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mmiloch.aopdemo.dao.AccountDAO;

public class AfterReturningDemoApp {

	public static void main(String[] args) {
	
		// read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		
		// get the bean from spring container
		AccountDAO myAccountDAO = context.getBean("accountDAO", AccountDAO.class);
		
		// call method to find the accounts
		List<Account> myAccounts = null;
		
		try {
			boolean tripWire = true;
			myAccountDAO.findAccounts(tripWire);	
		}catch(Exception exc) {
			System.out.println("\n\nMain Program ... caught exception: " + exc);
		}
		
		// display the accounts
		System.out.println("\n\nMain progeam: AfterThrowingDemApp");
		System.out.println("---");
		
		System.out.println(myAccounts);
		
		System.out.println("\n");
		
		
		// close the context
		context.close();

	}

}
