package com.mmiloch.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class MmilochAopExpressions {

	@Pointcut("execution(* com.mmiloch.aopdemo.dao.*.*(..))")
	public void forDaoPackage() {}
	
	@Pointcut("execution(* com.mmiloch.aopdemo.dao.*.get*(..))")
	public void getter() {}

	@Pointcut("execution(* com.mmiloch.aopdemo.dao.*.set*(..))")
	public void setter() {}
	
	@Pointcut("forDaoPackage() && !(getter() || setter())")
	public void forDaoPackageNoGetterSetter() {}
	
}
