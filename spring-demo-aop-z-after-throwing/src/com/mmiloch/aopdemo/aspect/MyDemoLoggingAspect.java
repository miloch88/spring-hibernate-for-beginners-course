package com.mmiloch.aopdemo.aspect;

import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.mmiloch.aopdemo.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {
	
	@AfterThrowing(pointcut="execution(* com.mmiloch.aopdemo.dao.AccountDAO.findAccounts(..))", throwing="theExc") 
	public void afterThrowingFindAccountAdvice(JoinPoint myJoinPoint, Throwable theExc ) {
		
		String method = myJoinPoint.getSignature().toLongString();
		System.out.println("\n====>>> Executing @AfterThrowing on method" + method);
		
		System.out.println("\n====>>> The ecveption is: " + theExc);
		
	}

	
	@AfterReturning(pointcut="execution(* com.mmiloch.aopdemo.dao.AccountDAO.findAccounts(..))", returning = "result")
	public void afterReturningFindAccountAdvice(JoinPoint myjJoinPoint, List<Account> result) { 
		
		String method = myjJoinPoint.getSignature().toLongString();
		System.out.println("\n====>>> Executing @AfterReturning on method" + method);
		
		System.out.println("\n====>>> result is: " + result);
		
		convertAccountNamesToUpperCase(result);
		
	}
	
	private void convertAccountNamesToUpperCase(List<Account> result) {
		
		for (Account tempAccount: result) {
			String theUpperName = tempAccount.getName().toUpperCase();
			
			tempAccount.setName(theUpperName);
		}
		
	}

	@Before("com.mmiloch.aopdemo.aspect.MmilochAopExpressions.forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint theJoinPoint) {		
		System.out.println("\n=====>>> Executing @Before advice on method");	
		
		// display the method signature
		MethodSignature methodSignature = (MethodSignature) theJoinPoint.getSignature();
		
		System.out.println("Method: " + methodSignature);
		
		// display method arguments
		
		
		Object[] args = theJoinPoint.getArgs();
		
		for(Object tempArg: args) {
			System.out.println(tempArg);
			
			if( tempArg instanceof Account) {
				// downcast and print Account stuff
				Account theAccount = (Account) tempArg;
				
				System.out.println("accoutn name: " + theAccount.getName());
				System.out.println("accoutn name: " + theAccount.getLevel());
			}
		}
		
	}
	
}
